//Soal 1
function range(start,finish){
    if(start && finish != null){
        var block = [];
        if(start > finish){
            //desc
            for(var i=start;i>=finish;i--){
                block.push(i);
            }
        } else {
            //asc
            for(var i=start;i<=finish;i++){
                block.push(i);
            }
        }
        return block;
    } else {
        return -1;
    }
}

console.log("Soal 1 :")
console.log(range(1, 10))
console.log(range(1))
console.log(range(11,18))
console.log(range(54, 50))
console.log(range())
console.log("\n")

//Soal 2
function rangeWithStep(start,finish,step){
    if(start && finish && step!= null){
        var block = [];
        if(start > finish){
            //desc
            for(var i=start;i>=finish;i-=step){
                block.push(i);
            }
        } else {
            //asc
            for(var i=start;i<=finish;i+=step){
                block.push(i);
            }
        }
        return block;
    } else {
        return -1;
    }
}

console.log("Soal 2 :")
console.log(rangeWithStep(1, 10, 2))
console.log(rangeWithStep(11, 23, 3))
console.log(rangeWithStep(5, 2, 1))
console.log(rangeWithStep(29, 2, 4))
console.log("\n")

//Soal 3
function sum(start,finish,step){
        var block = [];
        var s = 0;
        
        if(step == null){
            step = 1;
        }

        if(start > finish){
            //desc
            for(var i=start;i>=finish;i-=step){
                block.push(i);
            }
        } else if(start < finish) {
            //asc
            for(var i=start;i<=finish;i+=step){
                block.push(i);
            }
        } else {
            for(var i=0;i<=start;i++){
                block.push(i);
            }
        }

        for(var i=0;i<block.length;i++){
            s += block[i];
        }

        return s;
}

console.log("Soal 3 :")
console.log(sum(1,10))
console.log(sum(5,50,2))
console.log(sum(15,10))
console.log(sum(20,10,2))
console.log(sum(1))
console.log(sum())
console.log("\n")

//Soal 4
function dataHandling(a){
    for(var i=0;i<a.length;i++){
        for(var j=0;j<a[i].length;j++){
            if(j == 0){
                console.log("Nomor ID : " + a[i][j])
            } else if(j == 1){
                console.log("Nama Lengkap : " + a[i][j])
            } else if(j == 2){
                console.log("TTL : " + a[i][j] + " " + a[i][j+1])
            } else if(j == 3){
                continue;
            } else if (j == 4){
                console.log("Hobi : " + a[i][j])
            }
        }
        process.stdout.write("\n")
    }
    return "\n";
}

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

console.log("Soal 4 : ")
console.log(dataHandling(input))

//Soal 5
function balikKata(word){
    var w="";
    for(var i=word.length-1;i>=0;i--){
        w += word[i];
    }
    return w;
}
console.log("Soal 5 :")
console.log(balikKata("Kasur Rusak"))
console.log(balikKata("SanberCode"))
console.log(balikKata("Haji Ijah"))
console.log(balikKata("racecar"))
console.log(balikKata("I am Sanbers"))
console.log("\n")

//Soal 6
function dataHandling2(a){
    a.splice(1,2,"Roman Alamsyah Elsharawy","Provinsi Bandar Lampung")
    a.splice(4,1,"Pria","SMA Internasional Metro")
    console.log(a)

    var b = a[3];
    var c = b.split("/");
    switch(c[1]){
        case '01' :
            console.log("Januari")
            break;
        case '02' :
            console.log("Februari")
            break;
        case '03' :
            console.log("Maret")
            break;
        case '04' :
            console.log("April")
            break;
        case '05' :
            console.log("Mei")
            break;
        case '06' :
            console.log("Juni")
            break;
        case '07' :
            console.log("Juli")
            break;
        case '08' :
            console.log("Agustus")
            break;
        case '09' :
            console.log("September")
            break;
        case '10' :
            console.log("Oktober")
            break;
        case '11' :
            console.log("November")
            break;
        case '12' :
            console.log("Desember")
            break;
        default :
            console.log("Out Of Range")
            break;
    }
    
    var d = c.sort(function(val1,val2){return val2-val1})
    console.log(d)
    
    var e = d.join("-")
    console.log(e)

    var f = a[1];
    var g = f.slice(0,15)
    console.log(g)
    return "\n";
}
var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];

console.log("Soal 6 :")
console.log(dataHandling2(input))