//Soal 1
console.log("Soal 1 :");
function role (nama, peran){
    if (nama != "" && peran != ""){
        console.log("Selamat datang di Dunia Werewolf, " + nama);
        if(peran == "Penyihir"){
            console.log("Halo Penyihir " + nama + ", kamu dapat melihat siapa yang menjadi Werewolf!");  
        } else if (peran == "Guard"){
            console.log("Halo Guard " + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf.");
        } else if (peran == "Werewolf"){
            console.log("Halo Werewolf " + nama + ", kamu akan memakan mangsa setiap malam!");
        }
    } else if (peran == "" && nama != "") {
        console.log("Hai, " + nama + " pilih peranmu untuk memulai game!");
    } else if (nama == "" && peran != "") {
        console.log("Kamu memilih " + peran + ", untuk bermain Nama dan Peran harus diisi!");
    } else {
        console.log("Nama dan Peran harus diisi!");
    }
    console.log("\n")
}
role("","");
role("John","");
role("Jane", "Penyihir");
role("Jenita", "Guard");
role("Junaedi","Werewolf");
console.log("\n");

//Soal 2
var tahun = 1945;
var bulan = 1;
var tanggal = 21;

if (tahun >= 1900 && tahun <= 2200){
    if (tanggal >= 1 && tanggal <= 31){
        switch (bulan) {
            case 1 :
                bulan = "Januari";
                break;
            case 2 :
                bulan = "Februari";
                break;
            case 3 :
                bulan = "Maret";
                break;
            case 4 : 
                bulan = "April";
                break;
            case 5 :
                bulan = "Mei";
                break;
            case 6 :
                bulan = "Juni";
                break;
            case 7 :
                bulan = "Juli";
                break;
            case 8 :
                bulan = "Agustus";
                break;
            case 9 : 
                bulan = "September";
                break;
            case 10 :
                bulan = "Oktober";
                break;
            case 11 :
                bulan = "November";
                break;
            case 12 :
                bulan = "Desember";
                break;
            default :
                console.log("Bulan Tidak Terdaftar/Melebihi! Cek Kembali!");
            break;
        }
        console.log("Masukkan Tanggal dari 1-31!");        
    }
    console.log("Masukkan Tahun dari 1900-2200!");
}

console.log("Soal 2 :");
console.log(tanggal + " " + bulan + " " + tahun);