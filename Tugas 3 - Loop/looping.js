//Soal 1 :
var a = 0, b = 20;

console.log("Soal 1 :");
console.log("Looping Pertama :");
while(a < 20){
    a += 2;
    console.log(a + " - I Love Coding");
}

console.log("Looping Kedua :");
while(b != 0){
    console.log(b + " - I Will Become a Mobile Developer");
    b -= 2;
}
console.log("\n");

//Soal 2 :
var j,k;

console.log("Soal 2 :");   
for(var i=1;i<=20;i++){
    j = i%2;
    k = i%3;
    
    if(j != 0){
        if(k == 0){
            //if 3 and ganjil
            console.log(i + " - I Love Coding");
        } else {
            //ganjil
            console.log(i + " - Santai");
        }
    } else {
        //genap
        console.log(i + " - Berkualitas");
    }
}
console.log("\n");

//Soal 3 :
console.log("Soal 3 :");
for(var i=0;i<4;i++){
    for(var j=0;j<8;j++){
        process.stdout.write("#");
    }
    process.stdout.write("\n");
}
console.log("\n");

//Soal 4 :
console.log("Soal 4 :");
for(var i=0;i<8;i++){
    for(var j=0;j<i;j++){
        process.stdout.write("#");
    }
    process.stdout.write("\n");
}
console.log("\n");

//Soal 5 :
var a,b;

console.log("Soal 5 :");
for(var i=0;i<8;i++){
    a = i%2;
    if(a != 0){
        //row ganjil
        for(var j=0;j<8;j++){
            b = j%2;
            if(b != 0){
                //col ganjil
                process.stdout.write(" ");
            } else {
                //col genap
                process.stdout.write("#");
            }
        }
    } else {
        //row genap
        for(var j=0;j<8;j++){
            b = j%2;
            if(b != 0){
                //col ganjil
                process.stdout.write("#");
            } else {
                //col genap
                process.stdout.write(" ");
            }
        }
    }
    process.stdout.write("\n");
}