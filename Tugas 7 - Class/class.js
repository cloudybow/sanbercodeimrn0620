//Soal 1 :
class Animal {
    constructor (name){
        this._name = name
        this._legs = 4
        this._cold_blooded = false
    }

    //Getter & Setter NAME
    get name(){
        return this._name
    } set name(x){
        this._name = x
    }

    //Getter & Setter LEGS
    get legs(){
        return this._legs
    } set legs(x){
        this._legs = x
    }

    //Getter & Setter ColdBlood
    get cold_blooded(){
        return this._cold_blooded
    } set cold_blood(x){
        this._cold_blooded = x
    }
}

class Ape extends Animal {
    constructor (name){
        super(name)
        this._legs = 2
    }

    yell(){
        console.log("Auoooo")
    }
}

class Frog extends Animal {
    jump(){
        console.log("Hop Hop")
    }
}
var sheep = new Animal("shaun");

console.log("Soal 1 :")
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

var sungokong = new Ape("kera sakti")
sungokong.yell()

var kodok = new Frog("buduk")
kodok.jump() 

//Soal 2 :
class Clock {

    constructor ({template}) {
        this._template = template
    }
  
      render() {
      this.date = new Date();
  
      this.hours = this.date.getHours();
      if (this.hours < 10) this.hours = '0' + this.hours;
  
      this.mins = this.date.getMinutes();
      if (this.mins < 10) this.mins = '0' + this.mins;
  
      this.secs = this.date.getSeconds();
      if (this.secs < 10) this.secs = '0' + this.secs;
  
      this.output = this._template
        .replace('h', this.hours)
        .replace('m', this.mins)
        .replace('s', this.secs);
  
      console.log(this.output);
    }
  
      stop () {
      clearInterval(this.timer);
      }
  
      start() {
      this.render();
      this.timer = setInterval(this.render.bind(this), 1000);
    };
  }
  
  console.log("Soal 2 :")
  var clock = new Clock({template: 'h:m:s'});
  clock.start();