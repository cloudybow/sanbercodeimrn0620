var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

let call = (time, index) => {
    index > books.length-1 ? "" :
    readBooksPromise(time,books[index])
    .then (times => {return call(times, index+1)})
    .catch (error => {console.log(error)})
}

call(10000, 0)