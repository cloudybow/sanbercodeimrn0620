//Soal 1
function arrayToObject(arr){
    var now = new Date();
    var thisYear = now.getFullYear();
    var length = arr.length, final = [];
    
    if(!arr){
        return "";
    }

    //Conversion
    for ( var i = 0; i < length; i++ ) {
        var x = arr[i][3],k;
        if(x != null){
            k = thisYear - x;
            if(k < 0){
                k = "Invalid Birth Year";
            }
        } else {
            k = "Invalid Birth Year";
        }
        var item = {
            firstName: arr[i][0],
            lastName: arr[i][1],
            gender: arr[i][2],
            age : k
        };
     final.push(item);
    }

    //Output
    for(var i=0;i<final.length;i++){
        var x = final[i].firstName.concat(" " + final[i].lastName + " :");
        console.log(i+1 + ". " + x)
        console.log(final[i])
    }
    return "";
}

console.log("Soal 1 :")
var k = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ];
console.log(arrayToObject(k))
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
arrayToObject([])

//Soal 2
function shoppingTime(memberId, money) {
    if(memberId == '' && money){
        return "Mohon maaf, toko X hanya berlaku untuk member saja";
    } else if (money < 50000 && memberId){
        return "Mohon maaf, uang tidak cukup";
    } else if (!money && !memberId) {      
        return "Mohon maaf, toko X hanya berlaku untuk member saja";
    } else {
        var out = [],j,s=0,money2=money;
        while(money2 >= 50000) {
            if(money2 >= 1500000){
                out.push("Sepatu Stacattu");
                j = 1500000;
                money2 -= j;
            } else if(money2 >= 500000){
                out.push("Baju Zoro");
                j = 500000;
                money2 -= j;
            } else if(money2 >= 250000){
                out.push("Baju H&N");
                j = 250000;
                money2 -= j;
            } else if(money2 >= 175000){
                out.push("Sweater Uniklooh");
                j = 175000;
                money2 -= j;
            } else if(money2 >= 50000){
                out.push("Casing HP");
                j = 50000;
                money2 -= j;
            }
            if(out.includes("Casing HP") == true){
                break;
            }
        }

        var m = {memberId : memberId,
            money : money,
            listPurchased : out,
            changeMoney : money2};
        return m;
    }
}

console.log("Soal 2 :")
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

//Soal 3
function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var length = arrPenumpang.length,final = [];

    //Conversion
    for ( var i = 0; i < length; i++ ) {
        var start = arrPenumpang[i][1], finish = arrPenumpang[i][2];
        var locS = rute.indexOf(start), locF = rute.indexOf(finish);
        var sum = locF-locS, pay = sum*2000;

        var obj = {
            penumpang : arrPenumpang[i][0],
            dari : arrPenumpang[i][1],
            tujuan : arrPenumpang[i][2],
            bayar : pay
        }

        final.push(obj);
    }

    //Display
    for(var i=0;i<final.length;i++){
        console.log(final[i]);
    }

    return "";
}

console.log("Soal 3 :")
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));