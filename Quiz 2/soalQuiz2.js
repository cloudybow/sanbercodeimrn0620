/**
 * Berikut soal quiz kali ini, terdiri dari 3 Soal
 * Kerjakan dengan sebaik mungkin, dengan menggunakan metode yang telah dipelajari,
 * Tidak diperkenankan untuk menjawab hanya dengan console.log('teks jawaban');
 * maupun dengan terlebih dahulu memasukkannya ke dalam variabel, misal var a = 'teks jawaban'; console.log(a);
 * 
 * Terdapat tambahan poin pada setiap soal yang dikerjakan menggunakan sintaks ES6 (+5 poin)
 * Jika total nilai Anda melebihi 100 (nilai pilihan ganda + coding), tetap akan memiliki nilai akhir yaitu 100
 * 
 * Selamat mengerjakan
*/

/*========================================== 
  1. SOAL CLASS SCORE (10 poin + 5 Poin ES6)
  ==========================================
  Buatlah sebuah class dengan nama Score. class Score tersebut memiliki properti "subject", "points", dan "email". 
  "points" dapat di input berupa number (1 nilai) atau array of number (banyak nilai).
  tambahkan method average untuk menghitung rata-rata dari parameter points ketika yang di input berupa array (lebih dari 1 nilai)
*/

class Score {
  constructor (subject,point,email) {
    this.email = email
    this.subject = subject
    this.point = point
  }

  get points(){
    return this.point
  } set points(x){
    if(!x.isArray()){
      this.point = x
    } else {
      const average = () => {
        let a = point.length, b=0
        
        for(let i=0;i<a;i++){
          b+=point[i]
        }
        b /= a
    
        return b
      }
      this.point = average
    }
  }

  get subjects(){
    return this.subject
  } set subjects(x){
    this.subject = x
  }

  get emails(){
    return this.email
  } set subjects(x){
    this.email = x
  }
}

/*=========================================== 
  2. SOAL Create Score (10 Poin + 5 Poin ES6)
  ===========================================
  Membuat function viewScores yang menerima parameter data berupa array multidimensi dan subject berupa string
  Function viewScores mengolah data email dan nilai skor pada parameter array 
  lalu mengembalikan data array yang berisi object yang dibuat dari class Score.
  Contoh: 

  Input
   
  data : 
  [
    ["email", "quiz-1", "quiz-2", "quiz-3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88]
  ]
  subject: "quiz-1"

  Output 
  [
    {email: "abduh@mail.com", subject: "quiz-1", points: 78},
    {email: "khairun@mail.com", subject: "quiz-1", points: 95},
  ]
*/

const data = [
  ["email", "quiz-1", "quiz-2", "quiz-3"],
  ["abduh@mail.com", 78, 89, 90],
  ["khairun@mail.com", 95, 85, 88],
  ["bondra@mail.com", 70, 75, 78],
  ["regi@mail.com", 91, 89, 93]
]

const viewScores = (data, subject) => {
  let final = []
  const length = data.length, sb = data[0].indexOf(subject)

  for(let i=1;i<length;i++){
    const item = new Score(subject, data[i][sb], data[i][0])

    final.push(item)
  }

  return console.log(final)
}

// TEST CASE
console.log("QUIZ 1")
viewScores(data, "quiz-1")
console.log("QUIZ 2")
viewScores(data, "quiz-2")
console.log("QUIZ 3")
viewScores(data, "quiz-3")

/*==========================================
  3. SOAL Recap Score (15 Poin + 5 Poin ES6)
  ==========================================
    Buatlah fungsi recapScore yang menampilkan perolehan nilai semua student. 
    Data yang ditampilkan adalah email user, nilai rata-rata, dan predikat kelulusan. 
    predikat kelulusan ditentukan dari aturan berikut:
    nilai > 70 "participant"
    nilai > 80 "graduate"
    nilai > 90 "honour"

    output:
    1. Email: abduh@mail.com
    Rata-rata: 85.7
    Predikat: graduate

    2. Email: khairun@mail.com
    Rata-rata: 89.3
    Predikat: graduate

    3. Email: bondra@mail.com
    Rata-rata: 74.3
    Predikat: participant

    4. Email: regi@mail.com
    Rata-rata: 91
    Predikat: honour

*/

const recapScores = (data) => {
  let final = []
  const length = data.length

  for(let i=1;i<length;i++){
    const Email = data[i][0]
    const Arr = data[i].slice(1,4), l = Arr.length
    let a=0, Predikat

    //Hitung
    for(let i=0;i<l;i++){
      a += Arr[i]
    }
    a /= l
    let x = a.toFixed(1)
    
    //Predikat
    if(x > 70 && x <= 80){
      Predikat = "Participate"
    } else if (x > 80 && x <= 90){
      Predikat = "Graduate"
    } else if (x > 90){
      Predikat = "Honour"
    }

    const item = {
      Email,
      Rata : x,
      Predikat
    }

    final.push(item)
  }

  const out = (m=final) => {
    for(let i=0;i<m.length;i++){
      console.log(`${i+1}.`)
      console.log(`Email : ${m[i].Email}`)
      console.log(`Rata-Rata : ${m[i].Rata}`)
      console.log(`Predikat : ${m[i].Predikat}`)
    }
  }
  return out();
}

recapScores(data);